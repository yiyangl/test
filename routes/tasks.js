var User = require('../models/user');
var Task = require('../models/task');
const express = require('express');
const router = express.Router();

router.get('/', function(req, res) {
  Task.find(eval("(" + req.query.where + ")"))
      .sort(eval("(" + req.query.sort + ")"))
      .select(eval("(" + req.query.select + ")"))
      .skip(eval("(" + req.query.skip + ")"))
      .limit(eval("(" + req.query.limit + ")"))
      .then((tasks) => {
        if (req.query.count) { 
          if (req.query.count !== "false") {
            return res.status(200).json({
              message: "OK", 
              data: tasks.length
            });
          } else {
            return res.status(200).json({
              message: "OK", 
              data: tasks});
          }
        } else {
          return res.status(200).json({
            message: "OK", 
            data: tasks});
        }
      })
      .catch((err) => {
        return res.status(500).json({
          message: "Server error", 
          data: []});
      });
});

router.get('/:id', function(req, res) {
    Task.findById(req.params.id)
      .then((task) => {
        if (!task) {
          return res.status(404).json({message:"Task not found", data: []});
        } else {
          return res.status(200).json({message:"OK", data: task});
        }
      })
      .catch(function(error) {
        if (error.name === "CastError") {
          return res.status(404).json({
            message: 'Task not found',
            data: []
        });
      }
        return res.status(500).json(
                {
                message: 'Server error',
                data: []
                }
            )
        }
    )
  });
  
  
  router.delete('/:id', function(req, res) {
    Task.findByIdAndDelete(req.params.id)
    .then((task) => {
      if (task && task.assignedUser) {
        User.findById(task.assignedUser)
        .then((user) => {
          user.pendingTasks = 
          user.pendingTasks.filter(id => id.toString() !== req.params.id);
          user.save();
        })
        
        }
        if (task) {
          return res.status(200).json({message:"OK", data: task});
        } else {
          return res.status(404).json({message:"Task not found", data: []});
        }
    
    })
    
    .catch((error) => {
      if (error.name === "CastError") {
        return res.status(404).json({message:"Task not found", data: []});
      }
      return res.status(500).json({
        message: 'Server error',
        data: []
    });
    })  
  });
  
  
  router.put('/:id', function(req, res) {
    if (!req.body.name) {
      return res.status(400).json({
          message: 'Name not provided.',
          data: []
      })
  }
   if (!req.body.deadline) {
      return res.status(400).json({
          message: 'Deadline not provided.',
          data: []
      })
  }
    var newTask = {
      name: req.body.name,
      description: req.body.description,
      deadline: req.body.deadline,
      completed: req.body.completed,
      assignedUser: req.body.assignedUser,
      assignedUserName: req.body.assignedUserName,
      dateCreated: req.body.dateCreated
    }
    var betterTask = {
      name: req.body.name,
      description: req.body.description,
      deadline: req.body.deadline,
      completed: req.body.completed,
      assignedUser: "",
      assignedUserName: "unassigned",
      dateCreated: req.body.dateCreated
    }
    if (req.body.assignedUser && !req.body.completed) {
      User.findById(req.body.assignedUser)
      .then((user)=>{
        if (!user) {
          Task.findByIdAndUpdate(
            req.params.id, 
            {$set: betterTask}, 
            {returnOriginal: false})
            .then((tsk) => {
              if (!tsk) {
                return res.status(404).json({message:"Task not found", data: []});
              } else {
                if (betterTask.assignedUser) {
                  console.log(betterTask.assignedUser);
                  if (tsk.completed) {
                    console.log(tsk.completed);
                    
                      User.findByIdAndUpdate(betterTask.assignedUser, 
                        {$pull: {pendingTasks: tsk._id}})
                      .then(()=>{
                        return res.status(200).json({message:"OK", data: tsk});
                      })
                      .catch((err) => {
                        return res.status(404).json({message:"Assigned user not found for the task", data: tsk});
                      });
                  } else {
                    User.findByIdAndUpdate(betterTask.assignedUser, {$push: {pendingTasks: tsk._id}})
                    .then(() => {
                      return res.status(200).json({message:"OK", data: tsk});
                    })
                    .catch((err) => {
                      return res.status(404).json({message:"Assigned user not found for the task", data: tsk});
                    });
                  }
                } else {
                  return res.status(200).json({message:"OK", data: tsk});
                }
              }
            })
            .catch((error) => {
              if (error.name === "CastError") {
                return res.status(404).json({message:"Task not found", data: []});
              }
              return res.status(500).json({message:"Server error", data: []});
            });
        } else {
          Task.findByIdAndUpdate(
            req.params.id, 
            {$set: newTask}, 
            {returnOriginal: false})
            .then((tsk) => {
              if (!tsk) {
                return res.status(404).json({message:"Task not found", data: []});
              } else {
                if (newTask.assignedUser) {
                  console.log(newTask.assignedUser);
                  if (tsk.completed) {
                    console.log(tsk.completed);
                    
                      User.findByIdAndUpdate(newTask.assignedUser, 
                        {$pull: {pendingTasks: tsk._id}})
                      .then(()=>{
                        return res.status(200).json({message:"OK", data: tsk});
                      })
                      .catch((err) => {
                        return res.status(404).json({message:"Assigned user not found for the task", data: tsk});
                      });
                  } else {
                    User.findByIdAndUpdate(newTask.assignedUser, {$push: {pendingTasks: tsk._id}})
                    .then(() => {
                      return res.status(200).json({message:"OK", data: tsk});
                    })
                    .catch((err) => {
                      return res.status(404).json({message:"Assigned user not found for the task", data: tsk});
                    });
                  }
                } else {
                  return res.status(200).json({message:"OK", data: tsk});
                }
              }
            })
            .catch((error) => {
              if (error.name === "CastError") {
                return res.status(404).json({message:"Task not found", data: []});
              }
              return res.status(500).json({message:"Server error", data: []});
            });
        }
      })
    }    
  });

  //delete below
  router.post('/', function(req, res) {
    if (!req.body.name) {
      return res.status(400).json({
          message: 'Name not provided.',
          data: []
      })
  }
   if (!req.body.deadline) {
      return res.status(400).json({
          message: 'Deadline not provided.',
          data: []
      })
  }
    var newTask = {
      name: req.body.name,
      description: req.body.description,
      deadline: req.body.deadline,
      completed: req.body.completed,
      assignedUser: req.body.assignedUser,
      assignedUserName: req.body.assignedUserName,
      dateCreated: Date.now()
    }
    var betterTask = {
      name: req.body.name,
      description: req.body.description,
      deadline: req.body.deadline,
      completed: req.body.completed,
      assignedUser: "",
      assignedUserName: "unassigned",
      dateCreated: Date.now()
    }
  
    if (req.body.assignedUserName !== "unassigned" && req.body.assignedUser && !req.body.completed) {
      User.findById(req.body.assignedUser)
      .then((user)=> {
        if (user) {
          Task.create(newTask)
            .then((taskData)=>{
              User.findByIdAndUpdate(req.body.assignedUser, {$push: {pendingTasks: taskData._id}})
                .catch((error)=>{
                  return res.status(500).json({message: 'Server error5', data: []});
                })
              return res.status(201).json({message: 'Task created', data: taskData});
            })
            .catch((error) => {
              return res.status(500).json({message: 'Server error4', data: []});
            })
          
        } else {
          Task
          .create(betterTask)
          .then((taskData)=>{
            return res.status(201).json({message: 'Task created', data: taskData});
          })
          .catch((error) => {
            return res.status(500).json({message: 'Server error3', data: []});
          })
          console.log("reached here");
        }
      })
      .catch((error) => {
        return res.status(500).json({message: 'Server error2', data: []});
      })
    }
     else {
      Task.create(newTask)
            .then((taskData)=>{
              return res.status(201).json({message: 'Task created', data: taskData});
            })
            .catch((error) => {
              return res.status(500).json({message: 'Server error1', data: []});
            })
  }});
  
  module.exports = router;