var User = require('../models/user');
var Task = require('../models/task');
const express = require('express');
const router = express.Router();

router.get('/', function(req, res) {       
  User.find(eval("(" + req.query.where + ")"))
      .sort(eval("(" + req.query.sort + ")"))
      .skip(eval("(" + req.query.skip + ")"))
      .select(eval("(" + req.query.select + ")"))
      .limit(eval("(" + req.query.limit + ")"))
      .exec()
      .then((users) => {
        if (req.query.count) {
          if (req.query.count !== "false") {
            console.log(req.query.count !== "false");
            return res.status(200).json({
                message: "OK",
                data: users.length
              });
          }
          else {
            return res.status(200).json({message: "OK", data: users});
          }
        } else {
          return res.status(200).json({message: "OK", data: users});
        }
      })
      .catch((error) => {
        return res.status(500).json({
          message: "Server error!", 
          data: []
        });
      });
});

router.post('/', function(req, res) {
  if (!req.body.name) {
    return res.status(400).json({
        message: 'Name not provided.',
        data: []
    })
}
if (!req.body.email) {
    return res.status(400).json({
        message: 'Email not provided.',
        data: []
    })
}
  var newUser = {
    name: req.body.name,
    email: req.body.email,
    pendingTasks: req.body.pendingTasks,
    dateCreated: Date.now()
  }
  User.findOne({email: req.body.email})
    .then((user) => {
      if (!user) {
        User.create(newUser)
          .then((nn) => {
            return res.status(201).json({
              message: 'New user created', 
              data: nn
            });
          })
          .catch((error) => {
            return res.status(500).json({
              message: 'Server error', 
              data: []
            });
          });
      } else {
        return res.status(400).json({
          message:'Multiple users with the same email cannot exist', 
          data: []});
      }
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'Server error', 
        data: []});
    });
});
      
router.get('/:id', function (req, res) {
  User.findById(req.params.id)
  .then(function(user) {
      if (!user) {
          return res.status(404).json({
            message:"User not found", 
            data: []});
      } else {
          return res.status(200).json({
            message:"OK", 
            data: user});
      }
  })
  .catch(function(error) {
    if (error.name === "CastError") {
      return res.status(404).json(
        {
        message: 'User not found',
        data: []
        }
    )
    }
      return res.status(500).json(
              {
              message: 'Server error',
              data: []
              }
          )
      }
  )
});
      
router.delete('/:id', function(req, res) {

  User.findById(req.params.id)
  .exec()
  .then((user) => {
    if (!user) {
      return res.status(404).json({
        message: 'User not found',
        data: []
    });
    } else {
      Task.updateMany({assignedUser: req.params.id}, {assignedUser: "", assignedUserName: "unassigned"})
      .then(() => {
        user.delete()
        .then(()=>{
          return res.status(200).json({
            message: 'Deletion successed',
            data: []
        });
        })
      })
    }
  })
  .catch((err) => {
    if (err.name === "CastError") {
      return res.status(404).json({
        message: 'User not found',
        data: []
    });
    }
    return res.status(500).json({
      message: 'Server error',
      data: []
  });
  })
});
      
            router.put('/:id', function(req, res) {
            if (!req.body.name) {
                return res.status(400).json({
                    message: 'Name not provided.',
                    data: []
                })
            }
            if (!req.body.email) {
                return res.status(400).json({
                    message: 'Email not provided.',
                    data: []
                })
            }
        var newUser = {
          name: req.body.name,
          email: req.body.email,
          pendingTasks: req.body.pendingTasks,
          dateCreated: req.body.dateCreated
        }

        User.findOne({email: req.body.email})
            .then((user) => {
            if (user && user.id !== req.params.id) {
              return res.status(400).json({
                message:'Email already exist', 
                data: []
              });
            } else {
              User.findByIdAndUpdate(req.params.id, 
                {$set: newUser}, 
                {returnOriginal: false})
                .then((uuser) => {
                  if (uuser) {
                    return res.status(200).json(
                      {
                        message:"OK",
                        data: uuser
                      });
                  } else {
                    return res.status(404).json(
                      {
                        message:"User not found", 
                        data: []
                      });
                  }
                })
        }
    })
    .catch((err) => {
      return res.status(500).json(
        {
          message:"Server error", 
          data: []
        });
    });
      });
      
      module.exports = router;